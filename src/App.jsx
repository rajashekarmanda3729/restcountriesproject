import { Route, Routes, BrowserRouter as Router } from 'react-router-dom'
import React from 'react'
import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Header from './components/Header/index.jsx'
import Countries from './components/Countries/index.jsx'
import CountryDetailsCard from './components/CountryDetailsCard/index.jsx'
import NotFound from './components/NotFound/index.jsx'
import { getCountries } from './components/Server/fetchCountryData.jsx'
import './App.css'

export const DarkModeContext = React.createContext(null)

function App() {

  const [countries, setCountries] = useState([])
  const [searchInput, setSearchInput] = useState('')
  const [region, setRegion] = useState('')
  const [darkMode, setDarkMode] = useState(false)
  const [subRegion, setSubRegion] = useState('')
  const [sortBy, setSortBy] = useState('')
  const [isLoading, setIsLoading] = useState(true)
  const [order, setOrder] = useState('')

  React.useEffect(() => {
    darkMode ? document.body.style.backgroundColor = 'hsl(207, 26%, 17%)' : document.body.style.backgroundColor = 'White'
    getCountries().then(res => {
      setCountries(res)
      setIsLoading(false)
    })
  }, [darkMode])

  const countriesInitialList = countries.map(country => {
    return {
      id: uuidv4(),
      country: country.name.common,
      url: country.flags.png,
      population: country.population,
      region: country.region,
      capital: country.capital,
      subregion: country.subregion,
      area: country.area
    }
  })

  const searchCountry = (event) => setSearchInput(event.target.value)
  const changeRegion = (event) => (event.target.value == '') ? setRegion('') : setRegion(event.target.value)
  const sortingBy = (e) => (e.target.value == '') ? setSortBy('') : setSortBy(e.target.value)
  const togglingDarkMode = () => setDarkMode(prev => !prev)
  const changeSubRegion = (event) => (event.target.value != '') ? setSubRegion(event.target.value) : setSubRegion('')
  const onChangeOrder = (event) => setOrder(event.target.value)

  let filterCountriesList = countriesInitialList.filter(country => {
    if (country.subregion != undefined) {
      if (country.country.toLowerCase().includes(searchInput.toLowerCase()) &&
        country.region.toLowerCase().includes(region.toLowerCase()) &&
        country.subregion.includes(subRegion)) {
        return country
      }
    }
  })

  function sortingProcess(a, b) {
    if (order == 'Ascending') {
      if (a < b) return -1
      else if (a > b) return 1
      else return 0
    } else {
      if (a < b) return 1
      else if (a > b) return -1
      else return 0
    }
  }

  filterCountriesList = filterCountriesList.sort((a, b) => {
    if (sortBy == 'Name') return sortingProcess(a.country, b.country)
    else if (sortBy == 'Population') return sortingProcess(a.population, b.population)
    else if (sortBy == 'Area') return sortingProcess(a.area, b.area)
  })

  const subRegionsList = []
  countries.map(country => {
    if (!subRegionsList.includes(country.subregion) && country.subregion != undefined && country.region == region) {
      return subRegionsList.push(country.subregion)
    }
  })

  const sortProps = {
    sortingBy: sortingBy,
    sortBy: sortBy,
    darkMode: darkMode,
    region: region,
    changeRegion: changeRegion,
    subRegionDetailsIs: subRegion,
    changeSubRegion: changeSubRegion,
    subRegionsList: subRegionsList,
    searchCountry: searchCountry,
    searchInput: searchInput,
    isLoading: isLoading
  }

  return (
    <DarkModeContext.Provider value={[darkMode, order, onChangeOrder]}>
      <Router>
        < div className={`app`} >
          <Header darkMode={darkMode} togglingDarkMode={togglingDarkMode} />
          <Routes>
            <Route exact path='/' element={<Countries countriesData={filterCountriesList} sortProps={sortProps} />} />
            <Route exact path={`/name/:country`} element={<CountryDetailsCard />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </div >
      </Router>
    </DarkModeContext.Provider>
  )
}
export default App
