import axios from "axios";

const getCountriesData = axios.create({
    baseURL: 'https://restcountries.com/v3.1/',
})

export function getCountries() {
    return getCountriesData.get('/all').then(res => res.data)
}

export function getCountry(country) {
    const getCountryData = axios.create({
        baseURL: `https://restcountries.com/v3.1/`,
    })
    return getCountryData.get(`./name/${country}`).then(res => res.data)
}
