import React from 'react'
import moonsloid from '../../assets/moon-solid.svg'
import moonsolidDark from '../../assets/moon-regular_1.svg'
import { DarkModeContext } from '../../App'
import './headerStyle.css'

function Header(props) {

    const { togglingDarkMode } = props

    const [dark] = React.useContext(DarkModeContext)

    const toggleDarkMode = () => togglingDarkMode()
    let imageUrl, containerClass, headingClass, descriptionClass

    if (dark) {
        imageUrl = moonsolidDark
        headingClass = 'dark-heading'
        containerClass = 'dark-container'
        descriptionClass = 'dark-mode-description'

    } else {
        imageUrl = moonsloid
    }

    return (
        <div className={`header-container ${containerClass}`}>
            <h1 className={`header-heading ${headingClass}`}>Where in the world ?</h1>
            <div className='header-mode-container'>
                <p className={`mode-description ${descriptionClass}`} onClick={toggleDarkMode}><img
                    src={imageUrl} className='icon' />{dark ? 'Light Mode' : 'Dark Mode'}</p>
            </div>
        </div>
    )
}

export default Header