import React from 'react'
import { DarkModeContext } from '../../App'
import './notFoundStyle.css'

const NotFound = () => {

  const [dark] = React.useContext(DarkModeContext)

  return (
    <div className={`not-found-container ${dark ? 'dark-not-found-container' : ''}`}>
      <img
        src="https://assets.ccbp.in/frontend/react-js/not-found-blog-img.png"
        alt="not found"
        className="not-found-img"
      />
      <h1 className={`not-found-heading ${dark ? 'dark-not-found-heading' : ''}`}>Not Found: Please check URL</h1>
    </div>
  )
}

export default NotFound