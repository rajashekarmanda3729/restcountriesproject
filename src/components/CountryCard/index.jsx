import React from 'react'
import { Link } from 'react-router-dom'
import './countryCardStyle.css'
import { DarkModeContext } from '../../App'

function CountryCard(props) {

    const [dark] = React.useContext(DarkModeContext)
    const { country, region, url, population, capital, subregion, area } = props.countryDetails
    const contaierClassDark = dark ? 'dark-country-container' : ''
    const handleClick = () => console.log(country)

    return (
        <div className={`country-container ${contaierClassDark}`}>
            <Link to={`/name/${country}`} onClick={handleClick} style={{ textDecoration: 'none' }} >
                <img src={url} alt='flag' className='flag' />
                <h1 className={`country-name ${contaierClassDark}`}>{country}</h1>
                <p className={`population ${contaierClassDark}`}>Population : <span className={`data ${dark ? 'data-dark' : ''}`}>{population}</span></p>
                <p className={`population ${contaierClassDark}`}>Region : <span className={`data ${dark ? 'data-dark' : ''}`}>{region}</span></p>
                <p className={`population ${contaierClassDark}`}>subRegion : <span className={`data ${dark ? 'data-dark' : ''}`}>{subregion}</span></p>
                <p className={`population ${contaierClassDark}`}>Capital : <span className={`data ${dark ? 'data-dark' : ''}`}>{capital}</span></p>
                <p className={`population ${contaierClassDark}`}>Area : <span className={`data ${dark ? 'data-dark' : ''}`}>{area}</span></p>
            </Link>
        </div>
    )
}
export default CountryCard