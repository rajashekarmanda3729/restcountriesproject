import React from 'react'
import FilterOption from '../../components/FilterOption/index.jsx'
import { v4 as uuidv4 } from 'uuid'
import './arrangementStyle.css'
import { DarkModeContext } from '../../App.jsx'


function Arrangement(props) {

    const [dark, order, onChangeOrder] = React.useContext(DarkModeContext)

    const {
        sortBy,
        sortingBy,
        darkMode,
        region,
        changeRegion,
        subRegionDetailsIs,
        changeSubRegion,
        subRegionsList,
        searchInput,
        searchCountry
    } = props.sortProps

    const regionsArr = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania']
    const sortsArr = ['Name', 'Area', 'Population']
    const ordersArr = ['Ascending', 'Descending']

    return (
        <div className={`inputs-container`}>
            <input type='search' onChange={searchCountry} value={searchInput} className={`input-field search-icon ${dark ? 'dark-input-field' : ''}`} placeholder='search by country' />
            <button type='button' className={`search-button ${dark ? 'dark-input-field' : ''}`}>search</button>
            <select className={`select-field ${dark ? 'dark-select-field' : ''}`} onChange={changeRegion} value={region}>
                <option value=''>--Choose By Region--</option>
                {regionsArr.map(eachRegion => {
                    return <FilterOption name={eachRegion} key={uuidv4()} />
                })}
            </select>
            <select className={`select-field ${dark ? 'dark-select-field' : ''}`} value={subRegionDetailsIs} onChange={changeSubRegion}>
                <option value=''>--By subRegion--</option>
                {subRegionsList.map((eachSubRegion) => {
                    return <FilterOption name={eachSubRegion} key={uuidv4()} />
                })}
            </select>
            < select onChange={sortingBy} value={sortBy} className={`select-field ${dark ? 'dark-select-field' : ''}`}>
                <option>--Sort By--</option>
                {sortsArr.map(eachSortName => {
                    return <FilterOption name={eachSortName} key={uuidv4()} />
                })}
            </select >
            <select value={order} onChange={onChangeOrder} className={`select-field ${dark ? 'dark-select-field' : ''}`}>
                <option >--Select Order--</option>
                {ordersArr.map(eachOrder => {
                    return <FilterOption name={eachOrder} key={uuidv4()} />
                })}
            </select>
        </div>
    )
}

export default Arrangement