import React from 'react'
import Arrangement from '../Arrangement'
import CountryCard from '../CountryCard'
import Loader from 'react-loader-spinner'
import { DarkModeContext } from '../../App'
import './countriesStyle.css'

function Countries(props) {

    const { countriesData, sortProps } = props
    const { isLoading } = props.sortProps
    const [dark] = React.useContext(DarkModeContext)

    return (
        <>
            <Arrangement sortProps={sortProps} />
            <div className={`countries-container`}>
                {(countriesData.length != 0) ?
                    (countriesData.map(country => {
                        return <CountryCard key={country.id} countryDetails={country} dark={dark} />
                    })) :
                    (<>
                        {(!isLoading) && <div className={`countries-container`}>
                            <h1 className={`no-data-found ${dark ? 'dark-no-data-found' : ''}`}>Data not found with input match </h1>
                        </div>}
                        {isLoading && <Loader type='TailSpin' width={50} height={50} color={dark ? 'white' : 'black'} className='loader' radius={2} />}
                    </>)
                }
            </div>
        </>
    )
}
export default Countries