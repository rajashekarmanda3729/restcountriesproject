import React, { useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import Loader from 'react-loader-spinner'
import './countryDetailsStyle.css'
import arrowLeft from '../../assets/arrow-left-solid.svg'
import { getCountry } from '../../components/Server/fetchCountryData'
import { DarkModeContext } from '../../App'

function CountryDetailsCard() {
    let { country } = useParams()

    const [dark] = React.useContext(DarkModeContext)

    const [currentCountry, setCurrentCountry] = useState([])
    const [key, setKey] = useState('')

    React.useEffect(() => {
        getCountry(country).then(res => {
            setCurrentCountry((res))
            setKey(Object.keys(res[0].currencies)[0])
        })
    }, [country])

    return (
        (currentCountry.length == 0 ?
            (<Loader type='TailSpin' width={50} height={50} color={dark ? 'white' : 'black'} className='loader' radius={2} />) :
            <div className='contry-details-container'>
                <Link to='/'>
                    <button type='button' className={`back-button ${dark ? 'dark-back-button' : ''}`}>
                        <img src={arrowLeft} alt='arrow' className='arrow' /> Back</button>
                </Link>
                <div className='country-content-container'>
                    <img src={currentCountry[0].flags.png} alt='country' className='country-image' />
                    <div className='details-container'>
                        <h1 className={`country-main-heading ${dark ? 'dark-country-main-heading' : ''}`}>{currentCountry[0].name.common}</h1>
                        <div className="details-all-container">
                            <div>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Native Name: <span className='tag-name'>{currentCountry[0].name.official}</span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Population: <span className='tag-name'>{currentCountry[0].population}</span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Region: <span className='tag-name'>{currentCountry[0].region}</span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>subRegion: <span className='tag-name'>{currentCountry[0].subregion}</span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Capital: <span className='tag-name'>{currentCountry[0].capital[0]}</span></p>
                            </div>
                            <div>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Top Level Domains: <span className='tag-name'>{currentCountry[0].tld[0]}</span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Currencies: <span className='tag-name'>
                                    {currentCountry[0].currencies[key].name}
                                </span></p>
                                <p className={`tag ${dark ? 'dark-tag' : ''}`}>Languages: <span className='tag-name'>{
                                    Object.values(currentCountry[0].languages).reverse().join(',')
                                }</span></p>
                            </div>
                        </div>
                        <p className={`border-countries-container ${dark ? 'dark-country-main-heading' : ''}`}>
                            {`Border Countries:  `}
                            {(currentCountry[0].borders != undefined) && currentCountry[0].borders.map(eachBorder => {
                                return <button type='button' className={`country-button ${dark ? 'dark-country-button' : ''}`}>{eachBorder}</button>
                            })}
                        </p>
                    </div>
                </div>
            </div>
        )
    )
}

export default CountryDetailsCard