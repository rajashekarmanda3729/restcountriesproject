import './filterOptionStyle.css'

function FilterOption(props) {

    const { name } = props

    return (
        <option value={name}>{name}</option>
    )
}
export default FilterOption